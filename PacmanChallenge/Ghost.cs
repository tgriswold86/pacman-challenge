﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacmanChallenge
{
    public class Ghost
    {
        public bool Vulnerable { get; set; }
        public int Bonus { get; set; } = 200;

        public void Multibonus()
        {
            Bonus *= 2;
        }
    }
}
